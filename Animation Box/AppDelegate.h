//
//  AppDelegate.h
//  Animation Box
//
//  Created by Joe Sturzenegger on 11/18/13.
//  Copyright (c) 2013 Joe Sturzenegger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

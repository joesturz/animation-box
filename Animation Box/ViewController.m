//
//  ViewController.m
//  Animation Box
//
//  Created by Joe Sturzenegger on 11/18/13.
//  Copyright (c) 2013 Joe Sturzenegger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) UIView *square;

@end
int i = 0;
@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.square = [[UIView alloc] init];
    self.square.frame = CGRectMake(50.0f, 50.0f, 100.0f, 100.0f);
    self.square.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.square];
    
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPressed:)];
    [self.square addGestureRecognizer:gr];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) tapPressed:(UITapGestureRecognizer *) tapGr
{

    [UIView animateWithDuration:.25f animations:^{
        CGRect newFrame = self.square.frame;
        if (i == 0)
        {
            newFrame.origin.y += 300.0f;
            i++;
            
        }
        else if(i == 1)
        {
            newFrame.origin.x += 150.0f;
            i++;
        }
        else if(i == 2)
        {
            newFrame.origin.y -= 300.0f;
            i++;
        }
        else
        {
            newFrame.origin.x -= 150.0f;
            i = 0;
        }

        self.square.frame = newFrame;
        
        self.square.transform = CGAffineTransformRotate(self.square.transform, M_PI);
    }];
}
@end

//
//  main.m
//  Animation Box
//
//  Created by Joe Sturzenegger on 11/18/13.
//  Copyright (c) 2013 Joe Sturzenegger. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
